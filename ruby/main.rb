require 'json'
require 'socket'
require_relative 'main/game.rb'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

class NoobBot
  def initialize(server_host, server_port, bot_name, bot_key)
    @bot_name = bot_name
    @game = Game.new(@bot_name)
    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp)
  end

  private

  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)
    #tcp.puts join_race(bot_name, bot_key, "germany", 1)
    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)
    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']
      case msgType
        when 'carPositions'
          @game.store_position(json)
          if @game.turbo?
            puts "Turbo!"
            tcp.puts turbo("Go go go!")
          elsif @game.turn
            tcp.puts switch(@game.turn)
          else
            tcp.puts throttle(@game.throttle)
          end
        else
          case msgType
            when 'join'
              puts 'Joined'
            when 'joinRace'
              puts 'Race joined'
            when 'yourCar'
              @name = msgData['name']
              puts "My car is: #{@name}"
            when 'gameInit'
              puts 'Game init'
              @game.game_init(json)
              #tcp.puts switch("Right")
            when 'gameStart'
              puts 'Race started'
            when 'lapFinished'
              puts 'Lap finished'
            when 'crash'
              puts 'Someone crashed'
              #break
            when 'gameEnd'
              puts 'Race ended'
            when 'finish'
              puts 'Game finished'
            when 'tournamentEnd'
              puts 'Tournament ended'
            when 'turboAvailable'
              puts "Turbo available"
              @game.turbo_available
            when 'spawn'
              who = msgData['name']
              puts "Back on track: #{who}"
              @game.crashed if who == @name
            when 'error'
              puts "ERROR: #{msgData}"
            else
              puts "Unhandled: #{msgType}"
          end
          tcp.puts ping_message
      end
    end
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def throttle(throttle)
    make_msg("throttle", throttle)
  end

  def ping_message
    make_msg("ping", {})
  end

  def switch(direction)
    make_msg("switchLane", direction)
  end

  def join_race(bot_name, bot_key, track_name, car_count)
    make_msg("joinRace", {:botId => {:name => bot_name, :key => bot_key}, :trackName => track_name, :carCount => car_count})
  end

  def turbo(message)
    make_msg("turbo", message)
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end
end

NoobBot.new(server_host, server_port, bot_name, bot_key)
