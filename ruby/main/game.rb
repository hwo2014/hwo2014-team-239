require 'json'

class Game

  def initialize(me)
    @me = me
    @speed_constant = 3.5
    @max_angle = 60
    @high_speed = 10
    @safety_distance = 0
    @acceleration = 20
    @scale = [1.3187446675328003, 2.9067823381935405, 4.204324735203471, 5.264511343583974,
              6.130760991518017, 6.83855002282084, 7.416865193311651, 7.889390792930229, 8.275478610936801]
  end

  def game_init(json)
    message = JSON.parse(json)
    unless @track
      @track = message['data']['race']['track']['id']
      @pieces = message['data']['race']['track']['pieces']
      @lanes = message['data']['race']['track']['lanes']
      (0..(@pieces.size-1)).each do |i|
        if @pieces[i]['switch']
          @pieces[i]['switchLane'] = which_way_to_switch(i)
        end
      end
      set_speed_limits
      @my_positions = []
    end
    if message['data']['race']['raceSession']['laps']
      @laps = message['data']['race']['raceSession']['laps']
    else
      @laps = nil
    end
  end

  def set_speed_limits()
    (0..(@pieces.size-1)).each do |i|
      if @pieces[i]['radius']
        limits = @lanes.map { |lane| get_speed_limit(@pieces[i]['radius'] + (@pieces[i]['angle'] < 0 ? 1 : -1 ) * lane['distanceFromCenter']) }
        @pieces[i]['speedLimits'] = limits
      end
    end
  end

  def get_speed_limit(radius)
    0.035 * radius + @speed_constant
  end

  def which_way_to_switch(i)
    diff = (piece_length(i, 0, 0)) / 2 - (piece_length(i, 1, 1) / 2)
    while true
      i = i.next == @pieces.size ? 0 : i.next
      break if @pieces[i]['switch']
      diff += piece_length(i, 0, 0) - piece_length(i, 1, 1)
    end
      diff += (piece_length(i, 0, 0)) / 2 - (piece_length(i, 1, 1) / 2)
    case
      when diff > 0
        "Right"
      when diff < 0
        "Left"
      else
        nil
    end
  end

  def turn
    piece_index = current_position['pieceIndex']
    if ! @my_positions[-2] or @my_positions[-2]['pieceIndex'] != piece_index
      next_index = piece_index.next == @pieces.size ? 0 : piece_index.next
      lane_index = current_position['lane']['endLaneIndex']
      case @pieces[next_index]['switchLane']
        when 'Right'
          lane_index == @lanes.size-1 ? nil : 'Right'
        when 'Left'
          lane_index == 0 ? nil : 'Left'
        else
          nil
      end
    else
      nil
    end
  end

  def turbo_available()
    @turbo = true
  end

  def turbo?
    return false unless @turbo
    return false if current_position['angle'] > 5

    if last_straight? or someone_to_push?
      @turbo = false
      true
    else
      false
    end
  end

  def someone_to_push?
    current_position['data'].each do | car |
      next if car['id']['name'] == @me
      next_index = current_position['pieceIndex'].next == @pieces.size ? 0 : current_position['pieceIndex'].next
      in_next_piece = car['piecePosition']['pieceIndex'] == next_index
      no_switches = (not @pieces[current_position['pieceIndex']]['switch'] and not @pieces[next_index]['switch'])
      same_lane = car['piecePosition']['lane']['startLaneIndex'] == current_position['lane']['startLaneIndex'] and
          car['piecePosition']['lane']['endLaneIndex'] == current_position['lane']['endLaneIndex']
      return true if in_next_piece and same_lane and no_switches
    end
    false
  end

  def last_straight?
    return false if @laps and current_position['lap'] < @laps - 1
    ((current_position['pieceIndex'])..(@pieces.size-1)).each do |index|
      return false if @pieces[index]['speedLimits']
    end
    true
  end

  def store_position(json)
    hash = JSON.parse(json)
    my_position = hash['data'].select {|v| v['id']['name'] == @me }.first
    @my_positions << my_position['piecePosition']
    current_position['data'] = hash['data']
    current_position['gameTick'] = hash['gameTick'] ? hash['gameTick'] : 0
    current_position['angle'] = my_position['angle']
    current_position['lap'] = my_position['piecePosition']['lap']
    if @my_positions.size < 2
      current_position['speed'] = 0
      current_position['distance'] = 0
    else
      current_position['speed'] = current_speed
      current_position['distance'] = distance(current_position['pieceIndex'], current_position['lane']['startLaneIndex'], current_position['inPieceDistance'])
      @curve_max = current_position if current_position['angle'].abs >= previous_position['angle'].abs
      if current_position['pieceIndex'] != previous_position['pieceIndex'] and previous_piece['angle']
        if current_piece['length'] or current_piece['angle'] * previous_piece['angle'] < 0
          tune_speed_limits if @curve_max and @curve_max['angle'].abs < (@max_angle * 0.9)
          @curve_max = nil
        end
      end
      scale if current_position['speed'] < previous_position['speed'] and not @end_of_start
    end
  end

  def tune_speed_limits
    return if @laps
    if @skip_next_upgrade
      @skip_next_upgrade = nil
      return
    end
    #((@curve_max['pieceIndex']+2)..(previous_position['pieceIndex'])).each do | index |
    #  puts "Removing speed limit from #{index}" if @pieces[index].delete('speedLimits')
    #end
    upgrade = 1 + (@max_angle - @curve_max['angle'].abs) / 10 * 0.01
    upgrade = 1.3 if @curve_max['angle'] == 0
    index = previous_position['pieceIndex']
    while @pieces[index]['speedLimits'] and index >= 0
      puts "Upgrade speed limits of piece #{index} by factor #{upgrade}"
      @pieces[index]['speedLimits'].map! { | limit | limit * upgrade }
      break if not @pieces[index]['angle'] or not @pieces[index-1]['angle'] or @pieces[index]['angle'] * @pieces[index-1]['angle'] < 0
      index -= 1
    end
  end

  def throttle
    #exit if current_position['pieceIndex'] > 8
    return 1 if @my_positions.size<2
    limit, distance = next_speed_limit
    if distance > 0
      throttle = time_to_stop(limit, distance) ? 0 : 1
    else
      throttle = can_we_go(limit) ? 1 : 0
    end
    #puts "#{current_position['gameTick']} #{current_position['speed']} #{speed_limit} #{limit} #{distance} #{current_position['angle']} #{throttle} #{current_position['pieceIndex']}"
    throttle
  end

  def scale
    @end_of_start = @my_positions.size-2
    scale_index = [9, @end_of_start/10].min
    index = start.index{ |a| a['gameTick'] >= scale_index*10 }
    @high_speed = (@my_positions[index]['speed']/@scale[scale_index-1]) * 10
    puts "High speed set to #{@high_speed}"
  end

  def speed_limit
    limits = @pieces[current_position['pieceIndex']]['speedLimits']
    return @high_speed unless limits
    length = piece_length(current_position['pieceIndex'], current_position['lane']['startLaneIndex'], current_position['lane']['endLaneIndex'])
    lane_index = current_position['inPieceDistance'] < length/2 ? current_position['lane']['startLaneIndex'] : current_position['lane']['endLaneIndex']
    limits[lane_index]
  end

  def next_speed_limit
    index = current_position['pieceIndex']
    lane_index = current_position['lane']['endLaneIndex']
    distance = 0
    until @pieces[index]['speedLimits']
      distance += piece_length(index, lane_index, lane_index)
      index = index.next == @pieces.size ? 0 : index.next
    end
    [@pieces[index]['speedLimits'][lane_index], [distance - current_position['inPieceDistance'], 0].max]
  end

  def time_to_stop(limit, distance)
    return false if @my_positions.size < @acceleration
    scale = current_position['speed'] / @high_speed
    delta = current_position['speed'] - limit
    index = start.index{ |a| a['speed'] * scale > delta and a['speed'] > 0 }
    index = @end_of_start unless index
    distance + @safety_distance < current_position['speed'] * index
  end

  def can_we_go(limit)
    return true if @my_positions.size < @acceleration
    delta = limit - current_position['speed']
    index = start.index{ |a| a['speed'] > current_position['speed'] and a['speed'] > 0 }
    index = @end_of_start unless index
    if index and (@my_positions[index]['speed'] - @my_positions[index-1]['speed']) < delta
      piece_index = current_position['pieceIndex']
      lane_index = current_position['lane']['endLaneIndex']
      next_index = piece_index.next == @pieces.size ? 0 : piece_index.next
      if  @pieces[next_index]['speedLimits']
        next_limit = @pieces[next_index]['speedLimits'][lane_index]
        distance = piece_length(current_position['pieceIndex'], lane_index, lane_index) - current_position['inPieceDistance']
        not time_to_stop(next_limit, distance)
      else
        true
      end
    else
      false
    end
  end

  def start
    @end_of_start ? @my_positions[0..@end_of_start] : @my_positions
  end

  def current_position
    @my_positions.last
  end

  def previous_position
    @my_positions[-2]
  end

  def current_piece
    @pieces[current_position['pieceIndex']]
  end

  def previous_piece
    @pieces[previous_position['pieceIndex']]
  end

  def lane_distance(index)
    @lanes[index]['distanceFromCenter']
  end

  def distance(index, lane_index, in_peace_distance)
    sum = 0
    (0..(index-1)).each { |i| sum += piece_length(i, lane_index, lane_index)}
    sum + in_peace_distance
  end

  def piece_length(index, start_lane, end_lane)
    if @pieces[index]['length']
      Math.sqrt(@pieces[index]['length']**2 + (lane_distance(start_lane) - lane_distance(end_lane))**2)
    else
      sign = @pieces[index]['angle'] > 0 ? -1 : 1
      if start_lane == end_lane
        radius = @pieces[index]['radius'] + sign * lane_distance(start_lane)
        Math::PI * @pieces[index]['angle'].abs/180 * radius
      else
        center_length = Math::PI * @pieces[index]['angle'].abs/180 * @pieces[index]['radius']
        Math.sqrt(center_length**2 + (lane_distance(start_lane) - lane_distance(end_lane))**2)
      end
    end
  end

  def current_speed
    delta = current_position['inPieceDistance'] - previous_position['inPieceDistance']
    if previous_position['pieceIndex'] != current_position['pieceIndex']
      previous_length = piece_length(previous_position['pieceIndex'], previous_position['lane']['startLaneIndex'], previous_position['lane']['endLaneIndex'])
      delta = (previous_length - previous_position['inPieceDistance']) + current_position['inPieceDistance']
    end
    delta / (current_position['gameTick'] - previous_position['gameTick'])
  end

  def crashed
    @skip_next_upgrade = true
    #@safety_distance += 10
    puts 'Decreasing speed limits'
    (0..(@pieces.size-1)).each do | index |
      if @pieces[index]['speedLimits']
        limits = (0..(@lanes.size-1)).map { | lane | @pieces[index]['speedLimits'][lane] * 0.9 }
        @pieces[index]['speedLimits'] = limits
      end
    end
  end

end
