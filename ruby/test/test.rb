require 'json'
require_relative '../main/game.rb'

def json(file)
  File.read(File.dirname(__FILE__) + '/testJSON/' + file).gsub("\n", "").gsub(/  */, " ")
end

def assert(a, b)
  raise "Should be " + a.to_s + ", but was " + b.to_s if a != b
end

game = Game.new(json("gameinit.json"), "Hasselhoff", 100)
assert("Hasselhoff", game.driver)
assert(-20, game.lane_distance(0))
assert(100, game.piece_length(0, 0))
assert(100, game.piece_length(1, 1))
assert(220*Math::PI*(22.5/180), game.piece_length(2, 2))

assert(100, game.max_speed(0, 0))
game.max_speed(0, 0, 0.5)
assert(50, game.max_speed(0, 0))
assert(100, game.max_speed(0, 1))

game.max_speed(1, 50, 0.5)
game.max_speed(2, 1, 0.5)
assert(50, game.max_speed(1, 1))
assert(50, game.max_speed(2, 100))

assert(1, game.car_positions(json("carPositions1.json")))
assert(0, game.current_position['inPieceDistance'])
assert(0, game.current_position['speed'])
assert(0, game.current_speed)

assert(1, game.car_positions(json("carPositions2.json")))
assert(50, game.current_position['inPieceDistance'])
assert(50, game.current_speed)

assert(1, game.car_positions(json("carPositions3.json")))
assert(50, game.max_speed(1, 50))
game.car_crashed
assert(45, game.max_speed(1, 50))
assert(0, game.current_speed)
assert(90*0.5, game.max_speed(1, 49))

assert(0, game.car_positions(json("carPositions4.json")))
assert(80, game.current_speed)

assert(1, game.car_positions(json("carPositions1.json")))
assert(((game.piece_length(2, 0)-30)/-4).round(2), game.current_speed.round(2))

name = 'races/indianapolis-Hasselhoff-100.tsv'
`rm #{name}`
game.create_tsv
assert(6, File.read(name).split("\n").size)

puts "Tests OK"
